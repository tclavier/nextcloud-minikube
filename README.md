# Un peu de vocabulaire

* service
* déploiement
* pod

# Questions

* installer minikube
* lancer le container nextcloud comme un service accessible sur le port 80
* `minikube service nextcloud-service` créer le user admin dans nextcloud vérfier dans /settings/admin/serverinfo que l'on utilise une base sqlite 3
* restart du container => data perdu
* créer un storage persistant pour y coller /var/www/html => l'application n'est pas 12 factor app !
* créer 4 instances de notre pod nextcloud => c'est vraiement la merde côté partage de data
* changer la base par un pg avec stokage persistant
* activer le module ingress `minikube addons enable ingress`
* configurer un endpoint ingress pour accéder au service nextcloud

il faut changer des éléments dans la conf de nextcloud, fichier config/config.php

    'trusted_domains' => 
    array (
      0 => 'nextcloud.k8s',
    ),
    'overwrite.cli.url' => 'http://nextcloud.k8s',

* kubctl exec

Options :
* ajouter un redis
* 

